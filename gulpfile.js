var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var runSequence = require('run-sequence');

gulp.task('less', function () {
    return gulp.src('./src/less/main.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('postCss', function(){
    var postcss = require('gulp-postcss');
    return gulp.src('./dist/css/main.css')
        .pipe( postcss([ require('autoprefixer'), require('cssnano') ]))
        .pipe( gulp.dest('dist/css/') ) ;}
);

gulp.task('watch', function() {
    runSequence('less', 'postCss');
    gulp.watch('src/less/*.less', function(){
        runSequence('less', 'postCss')
    });
});

gulp.task('default',  ['watch'] );